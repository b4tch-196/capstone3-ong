import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { Button, Container, Form, Modal, Row, Table } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import DashboardData from '../components/DashboardData';
import UserContext from '../UserContext';
import Fade from 'react-reveal';

export default function Dashboard() {

    const {user} = useContext(UserContext);
    const [dbcourses, setDBCourses] = useState([]);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');

    // For Modal
    const [showAddCourse, setShowAddCourse] = useState(false);
    const addCourseClose = () => setShowAddCourse(false);
    const addCourseShow = () => setShowAddCourse(true);

    function addCourse () {

      // e.preventDefault()

      fetch('https://damp-woodland-93035.herokuapp.com/courses/checkCourseName', {

        method: 'POST',
        headers: {

          'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
        
          name: name
        })
      })
      .then(res => res.json())
      .then(data => {

        if(data){

          Swal.fire({
            title: "Duplicate course name found!",
            icon: "info",
            text: "The course name you are trying to add is already used"
          });

        } else {

          fetch('https://damp-woodland-93035.herokuapp.com/courses', {

            method: 'POST',
            headers: {

              Authorization: `Bearer ${localStorage.getItem('token')}`,
              'Content-Type' : 'application/json'
            },
            body: JSON.stringify({

              name: name,
              description: description,
              price: price,
              image: image
            })
          })
          .then(res => res.json())
          .then(data => {
            console.log(data.name)
            if(data.name){

              Swal.fire({
                title: "Course Added!",
                icon: "success"
              });

            } else {

              Swal.fire({
                title: "Failed to add course.",
                icon: "error",
                text: "Something went wrong, please try again"
              });
            }
          })
        };
      }) 

      // Clear input fields
      setName('');
      setDescription('');
      setPrice('');
      setImage('');

      addCourseClose();
    };




    useEffect(() => {

      fetch('https://damp-woodland-93035.herokuapp.com/courses', {

        headers: {

          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {

        setDBCourses(data.map(course => {

          return (
            <DashboardData key={course._id} courseDBProp={course}/>
          )
        }))
      });
    }, [dbcourses])


  return (
    
    (user.id !== null && user.isAdmin) ?
    <Container>
      <h1 className='text-center mt-5 mb-5'>Admin Dashboard</h1>

        <div className='text-center mt-3 mb-3'>
          <Fade left>
          <Button variant='primary' style={{width: '20rem'}}onClick={addCourseShow}>Add Course</Button>
          </Fade>
        </div>


        <Row>
          <Fade bottom>
          <Table bordered>
            <thead>

              <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>

            </thead>

            <tbody>
              {dbcourses}
            </tbody>

          </Table> 
          </Fade>
        </Row>
      

      <Modal show={showAddCourse} onHide={addCourseClose} centered>


        <Modal.Header closeButton>
          <Modal.Title>Add Course</Modal.Title>
        </Modal.Header>


        <Modal.Body>
          <Form>


            <Form.Group className="mb-3" controlId="coursename">
              <Form.Label>Course Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter new course name"
                required
                value={name}
                onChange={e => setName(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="coursedescription">
              <Form.Label>Course Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter new course description"
                required
                value={description}
                onChange={e => setDescription(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="courseprice">
              <Form.Label>Course Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter new course price"
                required
                value={price}
                onChange={e => setPrice(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="courseimage">
              <Form.Label>Course Image Link</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter new course image link"
                value={image}
                onChange={e => setImage(e.target.value)}
              />
            </Form.Group>


          </Form>
        </Modal.Body>


        <Modal.Footer>
          <Button variant="secondary" onClick={addCourseClose}>
            Close
          </Button>
          <Button variant="primary"  type='submit' id='submitBtn' onClick={e => addCourse(e)}>
            Add
          </Button>
        </Modal.Footer>


      </Modal>

    </Container>

    :

    <Navigate to={'/courses'} />
  )
}
