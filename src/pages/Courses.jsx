import {useState, useEffect, useContext} from 'react';
import { Col, Container, Row } from "react-bootstrap";
import { Navigate } from 'react-router-dom';
import CourseCard from '../components/CourseCard';
import UserContext from '../UserContext';

export default function Courses() {

	const [courses, setCourses] = useState([]);
	const {user} = useContext(UserContext);


	// useEffect(() => {

	// 	fetch('https://damp-woodland-93035.herokuapp.com/courses/activeCourses')

	// 	.then(res => res.json())
	// 	.then(data => {
		
	// 		setCourses(data.map(course => {

	// 			return (
	// 				<CourseCard key={course._id} courseProp={course} />
					
	// 			)
	// 		}))
	// 	});

	// }, [])

	useEffect(() => {

		fetch('https://damp-woodland-93035.herokuapp.com/courses/activeCourses')

		.then(res => res.json())
		.then(data => {
			
			setCourses(data.map(course => {

				return <Col lg={4} md={6} sm={12} key={course._id}>
				<CourseCard courseProp={course} />
			</Col>
			}))
		});

	}, [])

	return (

		// <>
		// 	<Container>
		// 		<h1 className='text-center'>Courses Available:</h1>
		// 		<hr />

		// 		<Row>
		// 			{
		// 				courses.map(course => {
		// 					return <Col md={6} lg={4} sm={12} key={course._id}>
		// 						<CourseCard courseProp={course} />
		// 					</Col>
		// 				})
		// 			}
		// 		</Row>
		// 	</Container>
		// </>
		(user.isAdmin) ?
		<Navigate to={'/dashboard'}/>

		:
		
		<>
			<Container>
				<h1 className='text-center mt-5'>Courses Available:</h1>
				<hr  className='mt-5 mb-5'/>
				<Row>
					{courses}
				</Row>
			</Container>
		</>
	)
}