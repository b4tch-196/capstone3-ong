import React, { useState } from 'react';
import { useContext } from 'react';
import { useEffect } from 'react';
import { Card, Col, Container, Row, Button, Form, Modal } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Enrollments from '../components/Enrollments';
import Swal from 'sweetalert2';
import Fade from 'react-reveal';


export default function Profile() {

    const {user} = useContext(UserContext)
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [_id, setId] = useState('');

    // ENROLLMENTS
    const [enrollments, setEnrollments] = useState();

      // FOR MODAL
    const [showEdit, setShowEdit] = useState(false);
    const editClose = () => setShowEdit(false);
    const editShow = () => setShowEdit(true);


    // const {userId} = useParams();

    useEffect(() => {
        // console.log(userId);
        fetch(`https://damp-woodland-93035.herokuapp.com/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            
            setId(data._id)
            setFirstName(data.firstName)
            setLastName(data.lastName)
            setMobileNo(data.mobileNo)
            setEmail(data.email);       
        });
    }, []);


    useEffect(() => {

        fetch('https://damp-woodland-93035.herokuapp.com/users/enroll', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            setEnrollments(data.map(enrollment => {

                // setCourseId(enrollment.courseId)
                // setEnrolledOn(enrollment.enrolledOn)
                // setStatus(enrollment.status)

                // console.log(enrollment.courseId)
                return <Col md={4} key={enrollment._id}>
                <Enrollments  enrollmentProp={enrollment}/>
                </Col>
            }))
        })
    }, []);

    function editProfile () {

        fetch(`https://damp-woodland-93035.herokuapp.com/users/updateUserDetails`, {

            method: 'PUT',
            headers: {

                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({

                firstName: firstName,
                lastName: lastName,
                mobileNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
        })

        editClose()

        Swal.fire({
            title: "Profile Updated!",
            icon: "success"
          });
    }


  return (
    
    (user.id === null) ?
    <Navigate to='/login'/>

    :

    <Container>
        <Row>
            <h1 className='mt-5'>Profile Details</h1>
            <Col md={6} sm={12}>
                <Fade left>
                <Card className='mt-5 mb-5'>
                    <Card.Body>
                        <Card.Subtitle>User ID</Card.Subtitle>
                        <Card.Text className='mt-2'>{_id}</Card.Text>
                        <Card.Subtitle>First Name</Card.Subtitle>
                        <Card.Text className='mt-2'>{firstName}</Card.Text>
                        <Card.Subtitle>Last Name</Card.Subtitle>
                        <Card.Text className='mt-2'>{lastName}</Card.Text>
                        <Card.Subtitle>Mobile Number</Card.Subtitle>
                        <Card.Text className='mt-2'>{mobileNo}</Card.Text>
                        <Card.Subtitle>Email</Card.Subtitle>
                        <Card.Text className='mt-2'>{email}</Card.Text>
                        <Button onClick={editShow}>Edit</Button>
                    </Card.Body>
                    
                </Card>
                </Fade>
            </Col>
        </Row>

        <Row>
            <h1>Enrollments:</h1>
            {enrollments}
        </Row>

        <Modal show={showEdit} onHide={editClose} centered>
            <Modal.Header closeButton>
                <Modal.Title>Edit Profile</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>

                    <Form.Group className="mb-3" controlId="coursename">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                    type="text"
                    placeholder="Enter new course name"
                    required
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="coursedescription">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter new course description"
                        required
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                    />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="courseprice">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                        type="number"
                        placeholder="Enter new course price"
                        required
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                    />
                    </Form.Group>
                </Form>
            </Modal.Body>

            <Modal.Footer>
            <Button variant="secondary" onClick={editClose}>
                Close
            </Button>
            <Button variant="success"  type='submit' id='submitBtn' onClick={e => editProfile(e)}>
                Save
            </Button>
            </Modal.Footer>
        </Modal>
    </Container>
  )
}
