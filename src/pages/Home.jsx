import { Container, Row, Col, Card } from 'react-bootstrap';
import Banner from '../components/Banner';
import CourseCarousel from '../components/CourseCarousel';
import Fade from 'react-reveal';

export default function Home(){


	
	return(

		<>
			<Fade top>
			<Container className='banner-container text-center border rounded'>
				<Banner />
			</Container>
			</Fade>

			<Container>
				<Row>				
					<Col lg={7} sm={12}>
						<Fade left>
						<CourseCarousel />
						</Fade>
					</Col>
					

					<Col lg={4} sm={12}>
					<Fade right>
						<Card className='homeCard mb-5'>
      				<Card.Body>
								<Card.Text>A magician, also known as an enchanter/enchantress, mage, magic-user, sorcerer/sorceress, spell-caster, warlock, witch, or wizard, is someone who uses or practices magic derived from supernatural, occult, or arcane sources.									
								</Card.Text>
								<Card.Text>
								Magic, sometimes spelled magick, is the application of beliefs, rituals or actions employed in the belief that they can manipulate natural or supernatural beings and forces. It is a category into which have been placed various beliefs and practices sometimes considered separate from both religion and science.
								</Card.Text>
							</Card.Body>
    				</Card>
					</Fade>
					</Col>
				</Row>

			</Container>
		</>
	)
}


