import React from 'react';
import { Button, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Error() {
  return (

    <>
      <Container className='text-center'>   
        <h1>404 - Page Not Found</h1>
        <p>The page you're looking for does not exist.</p>
        <Button variant='primary' as={Link} to='/'>Home</Button>
      </Container>
    </>
  )
}
