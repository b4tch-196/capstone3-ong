import React, { useContext, useEffect, useState } from 'react';
import {Button, Container, Form} from 'react-bootstrap';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Fade from 'react-reveal';

export default function Register() {

    const {user} = useContext(UserContext);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState('');
    const history = useNavigate();

    function registerUser(e) {

      e.preventDefault();

      fetch('https://damp-woodland-93035.herokuapp.com/users/checkEmail', {
        method: 'POST',
        headers: {
          'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
          email: email
        })
      })
      .then(res => res.json())
      .then(data => {
        if(data){
          Swal.fire({
            title: "Duplicate email found!",
            icon: "info",
            text: "The email is already used"
          });

        } else {
          fetch('https://damp-woodland-93035.herokuapp.com/users', {
            method: 'POST',
            headers: {
              'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              mobileNo: mobileNo,
              email: email,
              password: password
            })
          })
          .then(res => res.json())
          .then(data => {
            if(data.email){
              Swal.fire({
                title: "Registration successful!",
                icon: "success",
                text: "Thank you for signing-up"
              });

              history('/login');

            } else {
              Swal.fire({
                title: "Registration failed.",
                icon: "error",
                text: "Something went wrong, please try again"
              });
            };
          })
        };
      })

      // clear input fields after clicking register button
      setFirstName('');
      setLastName('');
      setMobileNo('');
      setEmail('');
      setPassword('');

    };

    useEffect(() => {
      if(firstName !== '' && lastName !== '' && email !== '' && password !== '' && password.length >= 8 && mobileNo !== '' && mobileNo.length === 11){
        setIsActive(true);

      } else {
        setIsActive(false);
      }
    }, [firstName, lastName, mobileNo, email, password]);
    

  return (

    (user.id !== null) ?
    <Navigate to='/courses'/>

    :
    
    <Container>
      <h1 className='text-center mt-5 mb-5'>Register Here</h1>
      <Fade bottom>
      <Form onSubmit={e => registerUser(e)} className='registration-form mx-auto p-3 rounded-4' style={{width: '30rem'}}>


        <Form.Group controlId="firstName" className='mb-3 mt-3'>
          <Form.Label>First Name:</Form.Label>
          <Form.Control
          type='text'
          placeholder='Enter your first name'
          required
          value={firstName}
          onChange={e => setFirstName(e.target.value)}
          />
        </Form.Group>


        <Form.Group controlId="lastName" className='mb-3'>
          <Form.Label>Last Name:</Form.Label>
          <Form.Control
          type='text'
          placeholder='Enter your last name'
          required
          value={lastName}
          onChange={e => setLastName(e.target.value)}
          />
        </Form.Group>


        <Form.Group controlId="mobileNo" className='mb-3'>
          <Form.Label>Mobile Number:</Form.Label>
          <Form.Control
          type='number'
          placeholder='Enter your 11-digit mobile number'
          required
          value={mobileNo}
          onChange={e => setMobileNo(e.target.value)}
          />
        </Form.Group>


        <Form.Group controlId="email" className='mb-3'>
          <Form.Label>Email:</Form.Label>
          <Form.Control
          type='email'
          placeholder='Enter your email'
          required
          value={email}
          onChange={e => setEmail(e.target.value)}
          />
        </Form.Group>


        <Form.Group controlId="password" className='mb-4'>
          <Form.Label>Password:</Form.Label>
          <Form.Control
          type='password'
          placeholder='Password must be atleast 8 characters long'
          required
          value={password}
          onChange={e => setPassword(e.target.value)}
          />
        </Form.Group>

        <p className='mb-4'>Already have an account? <Link to="/login">Login Here</Link></p>

        {
          isActive ?
          <Button className='mt-1 mb-2' variant='success' type='submit' id='submitBtn'>Register</Button>

          :

          <Button className='mt-1 mb-2' variant='danger' type='submit' id='submitBtn' disabled>Register</Button>
        }
      </Form>
      </Fade>
    </Container>
  )
}
