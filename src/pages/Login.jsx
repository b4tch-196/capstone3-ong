import {useState, useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Fade from 'react-reveal';

export default function Login() {

	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(e){

		// Prevents page redirection via form submission
		e.preventDefault();

		fetch('https://damp-woodland-93035.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Mastery!"
				})

			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})
			}
		});

		setEmail('');
		setPassword('');
	};


	const retrieveUserDetails = (token) => {
		fetch('https://damp-woodland-93035.herokuapp.com/users/details',{
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	};

	useEffect(() => {
		if(email !== '' && password !== '' && password.length >= 8){
			setIsActive(true);
			
		} else {
			setIsActive(false);
		}

	}, [email, password]);


	return (

		(user.id !== null) ?
			<Navigate to="/courses"/>
		:
		<Container>
			<h1 className='text-center mt-5 mb-5'>Login</h1>
			<Fade bottom>
			<Form onSubmit={e => loginUser(e)}  className='login-form mx-auto p-3 rounded-4' style={{width: '30rem'}}> 


				<Form.Group controlId="userEmail" className='mb-3 mt-3'>
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter you email"
						required
						value={email}
						onChange={e => setEmail(e.target.value)}
					/>
				</Form.Group>


				<Form.Group controlId="password" className='mb-3'>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your password"
						required
						value={password}
						onChange={e => setPassword(e.target.value)}
					/>
					<Form.Text className="text-muted">
						Password must be a minimum of 8 characters.
					</Form.Text>
				</Form.Group>

				<p className='mb-4'>Not yet registered? <Link to="/register">Register Here</Link></p>

			{ 
				isActive ? //if

				<Button className="mt-1 mb-2" variant="success" type="submit" id="submitBtn">
					Login
				</Button>

				: //else

				<Button className="mt-1 mb-2" variant="danger" type="submit" id="submitBtn" disabled>
					Login
				</Button>
			}

			</Form>
			</Fade>
		</Container>
	)
};