import React from 'react';
import { Link } from 'react-router-dom';

export default function Banner() {
  return (

    <>
      <h1>ARCANUM</h1>
      <p>The journey to becoming the mage that you always wanted is very long, why not make it short?</p>
      <Link className='btn btn-primary' to={'/courses'}>View Courses</Link>
    </>
  )
}
