import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useContext } from 'react';
import { Button, Card, Col, Container, Image, Modal, Row } from 'react-bootstrap';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Fade from 'react-reveal';

export default function CourseView() {

    const {user} = useContext(UserContext);
    const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [image, setImage] = useState('');
    const {courseId} = useParams();
    const history = useNavigate();

    // FOR MODAL
    const [showEnroll, setShowEnroll] = useState(false);
    const enrollClose = () => setShowEnroll(false);
    const enrollShow = () => setShowEnroll(true);

    const enroll = (courseId) => {

        fetch('https://damp-woodland-93035.herokuapp.com/users/enroll', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })

        .then(res => res.json())
        .then(data => {
            
            if(data && user.isAdmin !== true){
                Swal.fire({
                    title: 'Successfully enrolled!',
                    icon: 'success',
                    text: 'Thank you for enrolling'
                })

                history('/courses');

            } else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again later'
                })
            }
        });
    };

    useEffect(() => {

        fetch(`https://damp-woodland-93035.herokuapp.com/courses/getCourse/${courseId}`)

        .then(res => res.json())
        .then(data => {
            
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
            setImage(data.image);
        });
    },[courseId])

  return (

    <>
        <Container className='courseView-container'>
            <Row>

                <Col md={6} sm={12}>
                    <Fade left>
                    <Card className='courseViewCard mb-5'>
                        <Card.Body>
                            <Image src={image} fluid />
                        </Card.Body>
                    </Card>
                    </Fade>
                </Col>

                <Col md={6} sm={12}>  

                    <Fade right>               
                    <Card className='courseViewCard p-3'>
                        <Card.Body>

                            <h1 className='mt-3 mb-5'>{name}</h1>
                            <Card.Title className='mt-3 mb-3'>Description:</Card.Title>
                            <Card.Text className='mb-5'>{description}</Card.Text>
                            <Card.Subtitle className='mb-4'>Price: ${price}</Card.Subtitle>                            

                            {
                                (user.id !== null) ?
                                <Button variant='primary' style={{width: '100%'}} onClick={enrollShow}>Enroll</Button>

                                :

                                <Link className='btn btn-danger' to='/login' style={{width: '100%'}}>You must log in to enroll</Link>
                            }

                        </Card.Body>
                    </Card>
                    </Fade> 
                </Col>

            </Row>

            <Modal show={showEnroll} onHide={enrollClose} centered>
                <Modal.Body className='fw-bold'>Are you sure?</Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={enrollClose}>No
                    </Button>
                    <Button variant="success" onClick={() => enroll(courseId)}>Yes
                    </Button>
                </Modal.Footer>
            </Modal>
        </Container>
    </>
  )
}
