import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Fade from 'react-reveal';

export default function CourseCard({courseProp}) {

    const {image, name, description, price, _id} = courseProp;


  return (

    // <Container className='courseCard-container'>
    //     <Row>
    //         <Col>
    //             <Card className='cardCourseCard mb-3 text-dark' as={Link} to={`/courseView/${_id}`}>

    //                 <Card.Img 
    //                 variant='top'
    //                 src={image}
    //                 />
    //                 <Card.Body>
    //                     <Card.Title className='fw-bold'>{name}</Card.Title>
    //                     <Card.Subtitle>Description:</Card.Subtitle>
    //                     <Card.Text>{description}</Card.Text>
    //                     <Card.Subtitle>Price:</Card.Subtitle>
    //                     <Card.Text>{price}</Card.Text>  
    //                 </Card.Body>

    //            
    //         </Col>
    //     </Row>
    // </Container>

    <>
      <Fade bottom>
        <Card className='courseCard mt-4 mb-4 p-3 rounded'>
            <Card.Img className='courseCardImage' src={image} />
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text style={{height: '7rem'}}>{description}</Card.Text>
                <Card.Title>Price: ${price}</Card.Title>
                <Link className='btn btn-primary mt-2' to={`/courseView/${_id}`} style={{width: '100%'}}>Details</Link>
            </Card.Body>
        </Card>
      </Fade>
    </>
  )
}
