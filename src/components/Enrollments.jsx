import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Card } from 'react-bootstrap';
import Fade from 'react-reveal';

export default function Enrollments({enrollmentProp}) {

  const {courseId, enrolledOn, status} = enrollmentProp
  const [name, setName] = useState('');


  useEffect(() => {

    fetch(`https://damp-woodland-93035.herokuapp.com/courses/getCourse/${courseId}`)

    .then(res => res.json())
    .then(data => {
        
        setName(data.name);
    });
},[courseId])

  return (

    <>
      <Fade left>
      <Card className='mt-5'>
          <Card.Body>
              <Card.Subtitle className='mb-3'>{name}</Card.Subtitle>
              <Card.Text className='mt-2'> Course ID: {courseId}</Card.Text>
              <Card.Text className='mt-2'>Enrolled On: {enrolledOn}</Card.Text>
              <Card.Text className='mt-2'>Status: {status}</Card.Text>
          </Card.Body>
      </Card>
      </Fade>
    </>
  )
}
