import React from 'react';
import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function DashboardData({courseDBProp}) {

  const {image, name, description, price, isActive, _id} = courseDBProp;

  const [editName, setEditName] = useState(name);
  const [editDescription, setEditDescription] = useState(description);
  const [editPrice, setEditPrice] = useState(price);
  const [editImage, setEditImage] = useState(image);

  // FOR MODAL
  const [showEdit, setShowEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const editClose = () => setShowEdit(false);
  const editShow = () => setShowEdit(true);
  const deleteClose = () => setShowDelete(false);
  const deleteShow = () => setShowDelete(true);


  const deleteCourse = () => {

    fetch(`https://damp-woodland-93035.herokuapp.com/courses/${_id}`, {

      method: 'DELETE',
      headers: {

        'Content-Type' : 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })
  }

  const activateCourse = () => {

    fetch(`https://damp-woodland-93035.herokuapp.com/courses/activateCourse/${_id}`, {

      method: 'PUT',
      headers: {

        'Content-Type' : 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })
  }

  const disableCourse = () => {

    fetch(`https://damp-woodland-93035.herokuapp.com/courses/disableCourse/${_id}`, {

      method: 'PUT',
      headers: {

        'Content-Type' : 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })
  }

  function editCourse () {

    fetch(`https://damp-woodland-93035.herokuapp.com/courses/updateCourse/${_id}`, {

      method: 'PUT',
      headers: {

        'Content-Type' : 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({

        name: editName,
        description: editDescription,
        price: editPrice,
        image: editImage
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

    })

    editClose()

    Swal.fire({
      title: "Course Updated!",
      icon: "success"
    });
  }


  return (

    <>
      <tr>
        <td>{name}</td>
        <td>{description}</td>
        <td>${price}</td>

        {
          (isActive) ?
          <td>Active</td>

          :

          <td>Inactive</td>
        }
        
        <td>
          <Button variant='primary' className='me-2 mt-2' onClick={editShow}>Edit</Button>
          <Button variant='danger' className='me-2 mt-2' onClick={deleteShow}>Delete</Button>

          {
            (isActive) ?
            <Button variant='warning' className='me-2 mt-2' onClick={() => disableCourse()}>Disable</Button>

            :

            <Button variant='success' className='me-2 mt-2' onClick={() => activateCourse()}>Enable</Button>
          }
          
        </td>

      </tr>

      <Modal show={showEdit} onHide={editClose} centered>


        <Modal.Header closeButton>
          <Modal.Title>Add Course</Modal.Title>
        </Modal.Header>


        <Modal.Body>
          <Form>


            <Form.Group className="mb-3" controlId="coursename">
              <Form.Label>Course Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter course name"
                value={editName}
                onChange={e => setEditName(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="coursedescription">
              <Form.Label>Course Description</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter course description"
                value={editDescription}
                onChange={e => setEditDescription(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="courseprice">
              <Form.Label>Course Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter course price"
                value={editPrice}
                onChange={e => setEditPrice(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="courseimage">
              <Form.Label>Course Image Link</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter course image link"
                value={editImage}
                onChange={e => setEditImage(e.target.value)}
              />
            </Form.Group>


          </Form>
        </Modal.Body>


        <Modal.Footer>
          <Button variant="secondary" onClick={editClose}>
            Close
          </Button>
          <Button variant="primary"  type='submit' id='submitBtn' onClick={e => editCourse(e)}>
            Update
          </Button>
        </Modal.Footer>


      </Modal>

      <Modal show={showDelete} onHide={deleteClose} centered>

        <Modal.Body>Are you sure?</Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={deleteClose}>No
          </Button>
          <Button variant="success" onClick={() => deleteCourse()}>Yes
          </Button>
        </Modal.Footer>

      </Modal>
    </>
  )
}
