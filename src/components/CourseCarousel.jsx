import React from 'react';
import { Carousel } from 'react-bootstrap';

export default function CourseCarousel() {

  
  return (
    
    <>
      <Carousel 
      className='courseCarousel mx-auto mb-3'
      controls={false}
      indicators={false}
      interval={3000}
      >

        <Carousel.Item>
          <img
            className="carousel-img d-block"
            src='https://res.cloudinary.com/kyohei/image/upload/v1661135287/capstone3-ong/Magic_101_dtsa0b.jpg'
            alt="First slide"
            
          />
          <Carousel.Caption>
            <h3>Mage start up</h3>
            <p>Learn the basic fundamentals of magic ranging from mana to basic spells.</p>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item>
          <img
            className="carousel-img d-flex"
            src='https://res.cloudinary.com/kyohei/image/upload/v1661138394/capstone3-ong/Necromancer_w7xhaa.jpg'
            alt="Second slide"
            
          />
          <Carousel.Caption>
            <h3>Raise your own army</h3>
            <p>Summon different types of undead and siphon the life out of everything with Necromancy.</p>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item>
          <img
            className="carousel-img d-flex"
            src='https://res.cloudinary.com/kyohei/image/upload/v1661163458/capstone3-ong/DemonMagic_jmlznx.jpg'
            alt="Third slide"
            
          />
          <Carousel.Caption>
            <h3>Unleash hell upon your enemies</h3>
            <p>Become the manifestation of hell with Demon Magic.</p>
          </Carousel.Caption>
        </Carousel.Item>

      </Carousel>
    </>
  )
}
