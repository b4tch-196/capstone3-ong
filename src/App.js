import './App.css';
import {useState, useEffect} from 'react';
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import CourseView from './components/CourseView';
import Dashboard from './pages/Dashboard';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Register from './pages/Register';


function App() {

  const [user, setUser] = useState({

    id: null,
    isAdmin: null

  });


  const unsetUser = () => {

    localStorage.clear();

  };


  useEffect(() => {

    fetch('https://damp-woodland-93035.herokuapp.com/users/details', {

      headers: {

        Authorization: `Bearer ${localStorage.getItem('token')}`
    }
    })

    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== 'undefined'){

        setUser({

          id: data._id,
          isAdmin: data.isAdmin
        })

      } else {

        setUser({

          id: null,
          isAdmin: null
        })
      }
    });
  }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>

          <Routes>
            <Route exact path="*" element={<Error/>}/>
            <Route exact path="/" element={<Home/>}/>
            <Route exact path="/register" element={<Register/>}/>
            <Route exact path="/login" element={<Login/>}/>
            <Route exact path="/logout" element={<Logout/>}/>
            <Route exact path="/profile" element={<Profile/>}/>
            <Route exact path="/dashboard" element={<Dashboard/>}/>
            <Route exact path="/courses" element={<Courses/>}/>
            <Route exact path="/courseView/:courseId" element={<CourseView/>}/>
          </Routes>
          
        </Container>
      </Router>
    </UserProvider>
  )
};

export default App;
